#Analysis 


## Implementation notes 

Having built out the simple unit tests to verify that the regular expression was validating the provided postcodes correctly. I started with a simple implementation that only consisted of a couple of python files, then introduced a little more structure where useful. I opted not apply the usual rigour around TDD practice since the codebase was so volatile while performance tuning; approaching the task more like a set of technical spikes/ discovery style stories instead. This document covers the outcomes of those spikes. 

## The provided Regex

The regex provided in Part 1 correctly validates all of the example postcodes, however there are some edge cases that it does not handle correctly. I tested these using some custom unit tests:

```python
    def test_edge_case(self):
        validation_result = postcode_validator.validate('FIQQ 1ZZ', pre_compiled_expression)
        self.assertTrue(validation_result)
```

All of the rules in the [validation](https://en.wikipedia.org/wiki/Postcodes_in_the_United_Kingdom#Validation) section of the wiki are handled by the regular expression. But the UK postcodes that belong to overseas UK territories are not recognised as valid codes. Most of these have their own postcodes that have a slightly different structure that causes our expression to reject them:

UK Postcode | Edge case |
--------------------|------------------
AI-2640 | Anguilla  
ASCN 1ZZ | Ascension Islands  
STHL 1ZZ | Saint Helena  
DCU 1ZZ | Tristan da Cunha  
 BBND 1ZZ | British Indian Ocean Territory
BIQQ 1ZZ |	British Antarctic Territory
FIQQ 1ZZ | Faulkland Islands
PCRN 1ZZ | Pitcairn IslandsAI-2640 
SIQQ 1ZZ |South Georgia and the South Sandwich Islands|
TKCA 1ZZ | Turks and Caicos Islands

Some pseudo postcodes used for high profile organisations do not match either. The overseas UK code for Gibraltar does match as a valid postcode.	

## Performance

Although the standard libraries that I've used for the bulk import `csv` and  `re` are quite well optimized, it was important that my import was as efficient as possible for such a large set of data. I used the `datetime` module to monitor the performance of my code during certain tasks, for example:

```python
/usr/bin/python /Users/applemacbook/Projects/postcode-task/main.py
Benchmark import time is: 0:00:06.723658
Benchmark sort time is: 0:00:14.038151
Time after failed_validation.csv creation is0:00:14.075237
Time after failed_validation.csv creation is0:00:17.992739
Total run time is 0:00:17.992777

Process finished with exit code 0
```

The module gives a sufficiently accurate time and was appropriate since the task specifies using wallclock time as a baseline and not GPU time. My benchmark times varied slightly from run to run. I used my lower spec machine for performance tuning so that improvements where easier to measure, on my higher spec machine I achieved a run time of ~9 seconds for the full set of operations. 

####Regex 

Whilst I was completing part 2 I implemented one performance enhancement early. Since the same regular expression is being run against every row in the import data file I decided to compile the expression once before iterating. This allows the compiled expression to be kept in memory and used for the ~2 million iterations. Although it's often advised that for most situations caching a compiled regex expression in this way yields a negligible performance benefit; I was aware that the overhead of compiling the same expression 2 million times would be significant. At that stage I was just iterating through the [import data.csv](https://drive.google.com/file/d/0BwxZ38NLOGvoTFE4X19VVGJ5NEk/view?usp=sharing) and writing the results to the `failed_validation.csv` without sorting them. On my machine the task was taking ~10 seconds. Once I'd implemented the one time compile for the regex this runtime was reduced to ~5.5 seconds.

This can be tested by calling the `get_compiled_postcode_regex()` method within the bulk import loop. In fact, since the loop in my final project is now doing extra work to add the succeeded validation results to a new list, I found that the bulk import time is shaved in half by pre compiling the expression (from ~12.5 seconds to ~6.5 seconds on my machine). 

Once The full set of operations had been implemented, the total runtime was at around ~21 seconds and again nearly 6 seconds were added onto this runtime once I experimented with reverting the regex to compile within the loop. 

####Sorting
The standard `sorted()` method with the `key` parameter is the most efficient way to sort since python 2.4. I've used a simple lambda expresion to ensure that the list of tuples is ordered by the first element in each tuple, which is `row_id` integer in my case: 

```  
sorted_invalid_codes = 	sorted(invalid_postcodes, key=lambda x: x[0])

```

For versions of python older than 2.4 a Schwartzian transform would be the best replacement for this sorting optimization.

It's also worth noting that during the first few parts of the task I was checking the length of `invalid_postcodes` after the sort and after I had clipped the extra row for the column headers. I removed this call during performance tuning which shaved another second off the total runtime.  

####Looping
The overhead of `for` loops is fairly high in python and since I'm running a high volume of iterations, I wanted to find a way around it. I exprimented by swapping out the `for` loop iteration through the list that `csv.reader()` returns with two filters that call my `validate()` function to remove the unwanted entries from the invalid and valid postcode lists respectively. 

```python
filtered_invalid_postcodes = filter(lambda (row_id, postcode): validate(postcode, compiled_expression), file_reader)

```
Since the filestream had to be iterated twice this way, this was not beneficial for performance and was reverted. 

Since using the `.` operator to call an object attribute can also be expensive when repeated for many iterations, I refactored the append in my loop to call a pre-loaded version of the function so that the function reference is not reevalutaed 2 million times. This change gave a performance difference here of ~0.5-1 seconds wall time. 

```python
append_invalid = invalid_postcodes.append
    append_valid = valid_postcodes.append

    for row_id, postcode in file_reader:
        append_valid((row_id, postcode)) if validate(postcode, compiled_expression) \
            else append_invalid((row_id, postcode))
``` 

####Csv read and write
If I were writing a production project I would probably experiment with popular non-standard csv libraries such as `pandas`. For these purposes the read and write that `csv` provides seem relatively efficient but wasn't as lightweight as it could have been. I did experiment with some of the read and write method params with no success. Since `for` loops are a little expensive, I was hoping to use a list comprehension to either read or write my data however the these were not compatible for the tuple structure of my data so it started to look unlikey that I could use them without intoducing and uncessary extra step. 

####Parallel-procesing 
I considered parallel processing as a means to enhance the performance, however this would be non-trivial because the csv reader is unable to begin iteration from a row `n` in a csv, they must iterate through from the start of the file. This would mean that the only sensible set of parallel tasks you could arrange would be; one which adds invalid or valid records to queues and others which pick them off and add them to the respective csv. But this method felt like it could become difficult to manage. 

####Python version 

I did consider whether my code would run faster with python 2 or 3 and although the bulk import seems to actually take longer in python3 there is no consistent performance difference between the two in terms of total runtime because the list sorting is noticably quicker in python3. 
