# -*- coding: utf-8 -*-
import unittest
from postcode_validator import get_compiled_postcode_regex, validate

pre_compiled_expression = get_compiled_postcode_regex()


class TestPostcodeValidator(unittest.TestCase):

    def test__postcode_validator__validate__WhenCalledWithInvalidCharactersWillReturnFalse(self):

        validation_result = validate('$%±()()', pre_compiled_expression)

        self.assertFalse(validation_result)

    def test__postcode_validator__validate__WhenCalledWithFiveIdenticalCharactersWillReturnFalse(self):

        validation_result = validate('XX XXX', pre_compiled_expression)

        self.assertFalse(validation_result)

    def test__postcode_validator__validate__WhenCalledWithIncorrectInwardCodeLengthWillReturnFalse(self):

        validation_result = validate('A1 9A', pre_compiled_expression)
        self.assertFalse(validation_result)


    def test__postcode_validator__validate__WhenCalledWithoutSpaceWillReturnFalse(self):

        validation_result = validate('LS44PL', pre_compiled_expression)

        self.assertFalse(validation_result)

    def test__postcode_validator__validate__WhenCalledWithQInTheFirstPositionWillReturnFalse(self):

        validation_result = validate('Q1A 9AA', pre_compiled_expression)

        self.assertFalse(validation_result)

    def test__postcode_validator__validate__WhenCalledWithVInTheFirstPositionWillReturnFalse(self):

        validation_result = validate('V1A 9AA', pre_compiled_expression)

        self.assertFalse(validation_result)

    def test__postcode_validator__validate__WhenCalledWithXInTheFirstPositionWillReturnFalse(self):

        validation_result = validate('X1A 9BB', pre_compiled_expression)

        self.assertFalse(validation_result)

    def test__postcode_validator__validate__WhenCalledWithIInTheSecondPositionWillReturnFalse(self):

        validation_result = validate('LI10 3QP', pre_compiled_expression)

        self.assertFalse(validation_result)

    def test__postcode_validator__validate__WhenCalledWithJInTheSecondPositionWillReturnFalse(self):

        validation_result = validate('LJ10 3QP', pre_compiled_expression)

        self.assertFalse(validation_result)

    def test__postcode_validator__validate__WhenCalledWithZInTheSecondPositionWillReturnFalse(self):

        validation_result = validate('LZ10 3QP', pre_compiled_expression)

        self.assertFalse(validation_result)

    def test__postcode_validator__validate__WhenCalledWithQInTheThirdPositionWillReturnFalse(self):

        validation_result = validate('A9Q 9AA', pre_compiled_expression)

        self.assertFalse(validation_result)

    def test__postcode_validator__validate__WhenCalledWithCInTheFourthPositionWillReturnFalse(self):

        validation_result = validate('AA9C 9AA', pre_compiled_expression)

        self.assertFalse(validation_result)

    def test__postcode_validator__validate__WhenCalledWithAreaWithOnlySingleDigitDistrictsReturnsFalse(self):

        validation_result = validate('FY10 4PL', pre_compiled_expression)

        self.assertFalse(validation_result)

    def test__postcode_validator__validate__WhenCalledWithAreaWithOnlyDoubleDigitDistrictsReturnsFalse(self):
        validation_result = validate('SO1 4QQ', pre_compiled_expression)

        self.assertFalse(validation_result)

    def test__postcode_validator__validate__WhenCalledWithEC1A1BBReturnsTrue(self):
        validation_result = validate('EC1A 1BB', pre_compiled_expression)

        self.assertTrue(validation_result)

    def test__postcode_validator__validate__WhenCalledWithW1A0AXReturnsTrue(self):
        validation_result = validate('W1A 0AX', pre_compiled_expression)

        self.assertTrue(validation_result)

    def test__postcode_validator__validate__WhenCalledWithM11AEReturnsTrue(self):
        validation_result = validate('M1 1AE', pre_compiled_expression)

        self.assertTrue(validation_result)

    def test__postcode_validator__validate__WhenCalledWithB338THReturnsTrue(self):
        validation_result = validate('B33 8TH', pre_compiled_expression)

        self.assertTrue(validation_result)

    def test__postcode_validator__validate__WhenCalledWithCR26XHReturnsTrue(self):
        validation_result = validate('CR2 6XH', pre_compiled_expression)

        self.assertTrue(validation_result)

    def test__postcode_validator__validate__WhenCalledWithDN551PTReturnsTrue(self):
        validation_result = validate('DN55 1PT', pre_compiled_expression)

        self.assertTrue(validation_result)

    def test__postcode_validator__validate__WhenCalledWithGIR0AAReturnsTrue(self):
        validation_result = validate('GIR 0AA', pre_compiled_expression)

        self.assertTrue(validation_result)

    def test__postcode_validator__validate__WhenCalledWithSO109AAReturnsTrue(self):
        validation_result = validate('SO10 9AA', pre_compiled_expression)

        self.assertTrue(validation_result)

    def test__postcode_validator__validate__WhenCalledWithFY99AAReturnsTrue(self):
        validation_result = validate('FY9 9AA', pre_compiled_expression)

        self.assertTrue(validation_result)

    def test__postcode_validator__validate__WhenCalledWithWC1A9AAReturnsTrue(self):
        validation_result = validate('WC1A 9AA', pre_compiled_expression)

        self.assertTrue(validation_result)

    def test_edge_case(self):
        validation_result = validate('SW1W 0AA', pre_compiled_expression)
        self.assertTrue(validation_result)


