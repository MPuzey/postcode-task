import os
import csv
import sys


def file_exists(file_name):
    return os.path.isfile(file_name)


def delete_file(file_name):
    return os.remove(file_name)


def read_csv(file_name):
    return csv.reader(open(file_name), delimiter=",")


def write_to_csv(data, file_name):
    try:
        # If running in python 3 open file with newline param
        if sys.version_info >= (3, 0, 0):
            with open(file_name, 'w', newline='') as new_csv_file:
                __write_postcode_data(data, new_csv_file)
        else:
            with open(file_name, 'wb') as new_csv_file:
                __write_postcode_data(data, new_csv_file)

    except csv.Error:
        print('Failed to write to csv')


def __write_postcode_data(data, new_csv_file):

    csv_writer = csv.writer(new_csv_file)
    csv_writer.writerow(['row_id', 'postcode'])

    for row in data:
        csv_writer.writerow(row)
