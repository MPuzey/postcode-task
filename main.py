""" This is the entry point for my project and handles the bulk import. """
import datetime
from adapters.directory_adapter import file_exists, delete_file, write_to_csv
from postcode_validator import bulk_validator

if __name__ == '__main__':

    __INVALID_FILE_NAME = 'failed_validation.csv'
    __SUCCESS_FILE_NAME = 'succeeded_validation.csv'

    start_time = datetime.datetime.now()
    print('Bulk import in progress...')
    invalid_postcodes, valid_postcodes = bulk_validator()

    # Remove header from list
    del invalid_postcodes[0]
    del valid_postcodes[0]

    time_after_import = datetime.datetime.now()
    benchmark = time_after_import - start_time
    print('Benchmark import time is: ' + str(benchmark))

    sorted_invalid_codes = sorted(invalid_postcodes, key=lambda x: x[0])
    sorted_valid_codes = sorted(valid_postcodes, key=lambda x: x[0])

    time_after_sort = datetime.datetime.now()
    sort_time = time_after_sort - start_time
    print('Time after sort is: ' + str(sort_time))

    if file_exists(__INVALID_FILE_NAME):
        delete_file(__INVALID_FILE_NAME)

    if file_exists(__SUCCESS_FILE_NAME):
        delete_file(__SUCCESS_FILE_NAME)

    write_to_csv(sorted_invalid_codes, __INVALID_FILE_NAME)

    first_file_write = datetime.datetime.now() - start_time
    print('Time after failed_validation.csv creation is ' + str(first_file_write))

    write_to_csv(sorted_valid_codes, __SUCCESS_FILE_NAME)
    second_file_write = datetime.datetime.now() - start_time
    print('Time after failed_validation.csv creation is ' + str(second_file_write))

    total_runtime = datetime.datetime.now() - start_time
    print('Total run time is ' + str(total_runtime))







