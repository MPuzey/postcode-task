""" This module validates a string against a provided regular expression. The expression the module compiles matches
against valid postcodes but it can be extended to support more expressions. """

import re

from adapters.directory_adapter import read_csv

from provided_expression import regular_expression


def validate(string, compiled_expression):
    """ This function validates a string against a compiled regex expression. """

    match = compiled_expression.match(string)
    return True if match else False


def get_compiled_postcode_regex():
    """ This function fetches a regex expression and returns the compiled expression. """

    expression = regular_expression
    return re.compile(expression)


def bulk_validator():
    """ This function validates multiple postcodes from an import_data.csv. Regex is
    pre compiled once which speeds up the validation significantly (by ~5 seconds on my machine)."""

    file_reader = read_csv("import_data.csv")
    invalid_postcodes = []
    valid_postcodes = []

    compiled_expression = get_compiled_postcode_regex()

    append_invalid = invalid_postcodes.append
    append_valid = valid_postcodes.append

    for row_id, postcode in file_reader:
        if validate(postcode, compiled_expression):
            append_valid((row_id, postcode))
        else:
            append_invalid((row_id, postcode))

    return invalid_postcodes, valid_postcodes
