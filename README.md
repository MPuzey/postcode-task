#README

##Running the project

###Prerequisites 
For the purposes of the performance engineering section the final version of the project was modified to be compatible with python 3 (3.5.1 in my case), this was just a small change to how csv files were read. If you are running windows install python3 use the [installer](https://www.python.org/downloads/), run `apt-get install python3` or `brew install python3` to install python3 on linux and OSX respectively.

###Running the bulk import
Whether you're running a unix or windows based OS simply drop an unzipped copy of the [import_data.csv](https://drive.google.com/file/d/0BwxZ38NLOGvoTFE4X19VVGJ5NEk/view) into the project root next to the `main.py`, `cd` into the project root and run: 

```
python3 main.py 
```


###Unit tests
You can run the unit tests from the root directory of the unzipped directory using:
 
 ```
 python3 -m unittest discover -s tests 
 ```
 
I initially decided to represent the older versions of my project (Part 1 and 2 etc.) by checking out their git tags and turning them into independent modules in my project. Unfortunately this import structure prevented the terminal commands from running correctly so it was removed. If you want to view parts 1, 2 and 3 (before tuning) view my set of git tags with `git tag` and checkout the the older tasks `git checkout Task1`. To run the earlier parts from their tag you must have [python 2](https://www.python.org/downloads/) installed. I am using 2.7.11 but 2.4 or higher is definitely required. OSX comes with python 2 out of the box. To run the project with your install of python 2 use:
 
 ```
 python main.py
 ```
